<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<form action="/CHAdvancedConsole/AddWidget" method="post">

					<h3>General Configuration</h3>
					New Widget Id: <input type="text" name="newWidgetId" /><br />
					<br />

					<!-- Label for Channel Instance configuration data -->
					<h3>Channel Instance configuration data</h3>
					Adc Address: <input type="text" name="adcAddress" /> <br /> Alias:
					<input type="text" name="alias" /><br /> Type: <input type="text"
						name="type" /><br /> Default Locale: <input type="text"
						name="defLocale" /><br /> Description: <input type="text"
						name="description" /><br /> Hub Enabled: <input type="number"
						name="hubEnabled" value="1" /><br /> Enabled: <input type="number"
						name="enabled" value="1" /><br /> Tenant Id: <input type="text"
						name="tenantId" /><br /> <br />
					<br />

					<!-- Label for Widget configuration data -->
					<h3>Widget configuration data</h3>
					Logged ag wait cntct ratio:<input type="number"
						name="logged_ag_wait_cntcn_radio" value="0" /><br> Max chat
					workload per agent:<input type="number"
						name="max_chat_workload_per_agent" value="1" /><br> Cache
					eviction enabled:<input type="number" name="cache_eviction_enabled"
						value="1" /><br> chat_transcription_enabled:<input
						type="number" name="chat_transcription_enabled" value="1" /><br>
					cross_domain_enabled:<input type="number"
						name="cross_domain_enabled" value="0" /><br> hideonmobile:<input
						type="number" name="hideonmobile" value="0" /><br>
					hideoffline:<input type="number" name="hideoffline" value="0" /><br>
					origin_site:<input type="text" name="origin_site" /><br>
					SATISFACTION_RANGE:<input type="number" name="satisfaction_range"
						value="5" /><br> SHOWCOLLAPSED:<input type="number"
						name="showcollapsed" value="0" /><br>
					SITE_ACTIVATION_TIMEOUT:<input type="number"
						name="site_activation_timeout" value="0" /><br> TIMEOUT_MIN:<input
						type="number" name="timeout_min" value="2" /><br>
					<!--   ROW_UPDATE_DATE:<input type="text" name="origin_site"/><br> -->
					OFFLINE_CH_INSTANCE_ID:<input type="text"
						name="offline_ch_instance_id" /><br> OENG_CALENDAR_ID:<input
						type="number" name="oeng_calendar_id" value="1" /><br>
					UPGRADE_VIDEO_ENABLED:<input type="number"
						name="upgrade_video_enabled" value="1" /><br>
					UPGRADE_AUDIO_ENABLED:<input type="number"
						name="upgrade_audio_enabled" value="1" /><br>
					DIRECT_VIDEO_ENABLED:<input type="number"
						name="direct_video_enabled" value="0" /><br>
					DIRECT_AUDIO_ENABLED:<input type="number"
						name="direct_audio_enabled" value="0" /><br>
					MESSAGE_AUDIO_NOTIF_ENABLED:<input type="number"
						name="message_audio_notif_enabled" value="1" /><br>
					CO_BROWSING_ENABLED:<input type="number" name="co_browsing_enabled"
						value="1" /><br> AUTOMATICALLY:<input type="number"
						name="automatically" value="0" /><br> fields_metadata:<input
						type="number" name="fields_metadata" value="5" /><br></br>

					<!-- Label for Widget configuration data -->
					<h3>Widget Configuration trigs</h3>
					ACTIVATION_TIMEOUT:<input type="number" name="activation_timeout"
						value="0" /><br> PAGE_REGEX:<input type="text"
						name="page_regex" value=".*" /><br>

					<!-- Label for Widget configuration data -->
					<h3>Widget Squeue Configuration</h3>
					Page regex:<input type="text" name="page_regex_squeue" value=".*" /><br>
					<br />
					<br />

					<!-- Label for Widget Infrastructure Server configuration data -->
					<h3>Widget Infrastracture Server Configuration</h3>
					Infrastructure Server Id:<input type="number" name="infServerId"
						value="1" /><br> <br />
					<br />

					<!-- Label for Widget Infrastructure Server configuration data -->
					<h3>Channel ACD configuration</h3>
					AUTOMATIC:<input type="number" name="automatic" value="0" /><br>
					MAX_TRANSFER_ATTEMPT:<input type="number"
						name="max_transfer_attempt" value="3" /><br> <br />
					<br /> <input class="btn btn-secondaty" type="submit" value="Create Widget" />
				</form>
				
				 
			</div>
		</div>
		<!-- /#page-content-wrapper -->
