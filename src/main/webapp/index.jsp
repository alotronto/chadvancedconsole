<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<!--  Include header page -->
	<jsp:include page="header.jsp"/>

<body>
 	<div id="wrapper">
		<jsp:include page="sidebar.jsp"/>
		<jsp:include page="aboutContent.jsp"/>

		<jsp:include page="footer.jsp"/>
	</div>
</body>
</html>