package it.arl.chadvancedc.model;

public class Widget {
	
	private String ID;
	private String acd = "acd:";
	private String description = "OE channel instance for XXX subagent widget service";
	private int trigID;
	private int msgItID;
	private int msgEnID;
	private int stileID;
	private int sQueueID;
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getAcd() {
		return acd;
	}
	public void setAcd(String acd) {
		this.acd = acd;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTrigID() {
		return trigID;
	}
	public void setTrigID(int trigID) {
		this.trigID = trigID;
	}
	public int getMsgItID() {
		return msgItID;
	}
	public void setMsgItID(int msgItID) {
		this.msgItID = msgItID;
	}
	public int getMsgEnID() {
		return msgEnID;
	}
	public void setMsgEnID(int msgEnID) {
		this.msgEnID = msgEnID;
	}
	public int getStileID() {
		return stileID;
	}
	public void setStileID(int stileID) {
		this.stileID = stileID;
	}
	public int getsQueueID() {
		return sQueueID;
	}
	public void setsQueueID(int sQueueID) {
		this.sQueueID = sQueueID;
	}
	
	

}
