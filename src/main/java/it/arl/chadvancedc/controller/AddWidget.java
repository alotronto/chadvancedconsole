package it.arl.chadvancedc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.arl.chadvancedc.jpa.ChannelUtils;
import it.arl.chadvancedc.jpa.WidgetUtils;

/**
 * Servlet implementation class AddWidget
 */
@WebServlet("/AddWidget")
public class AddWidget extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private WidgetUtils widgetUtils = new WidgetUtils();
	private ChannelUtils channelUtils = new ChannelUtils();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddWidget() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		
		//Controller fucntion to insert new Channel Instance
		if(widgetUtils.addNewChInstance(
				request.getParameter("newWidgetId"),
				request.getParameter("adcAddress"),
				request.getParameter("alias"), 
				Integer.parseInt(request.getParameter("hubEnabled")),
				request.getParameter("type"), 
				request.getParameter("defLocale"), 
				request.getParameter("description"), 
				Integer.parseInt(request.getParameter("enabled")), 
				request.getParameter("tenantId")))
			out.println("New Channel Instance correctly inserted");
		else
			out.println("Error to insert new Channel Instance");
		
		
			
		if(widgetUtils.addOengWidConf(
				Integer.parseInt(request.getParameter("logged_ag_wait_cntcn_radio")), 
				Integer.parseInt(request.getParameter("max_chat_workload_per_agent")), 
				Integer.parseInt(request.getParameter("cache_eviction_enabled")), 
				Integer.parseInt(request.getParameter("chat_transcription_enabled")), 
				Integer.parseInt(request.getParameter("cross_domain_enabled")), 
				Integer.parseInt(request.getParameter("hideonmobile")), 
				Integer.parseInt(request.getParameter("hideoffline")), 
				request.getParameter("origin_site"), 
				Integer.parseInt(request.getParameter("satisfaction_range")), 
				Integer.parseInt(request.getParameter("showcollapsed")), 
				Integer.parseInt(request.getParameter("site_activation_timeout")), 
				Integer.parseInt(request.getParameter("timeout_min")), 
				request.getParameter("newWidgetId"), 
				request.getParameter("offline_ch_instance_id"),
				Integer.parseInt(request.getParameter("oeng_calendar_id")),
				Integer.parseInt(request.getParameter("upgrade_video_enabled")),
				Integer.parseInt(request.getParameter("upgrade_audio_enabled")), 
				Integer.parseInt(request.getParameter("direct_video_enabled")),
				Integer.parseInt(request.getParameter("direct_audio_enabled")),
				Integer.parseInt(request.getParameter("message_audio_notif_enabled")),
				Integer.parseInt(request.getParameter("co_browsing_enabled")), 
				Integer.parseInt(request.getParameter("automatically")),
				Integer.parseInt(request.getParameter("fields_metadata")))
				)
			out.println("New Configuration Widget correctly inserted");
		else
			out.println("Error to insert new Configuration Widget");
		
		if(widgetUtils.addNewTrigs(
				request.getParameter("newWidgetId"), 
				Integer.parseInt(request.getParameter("activation_timeout")), 
				request.getParameter("page_regex"))
				)
			out.println("New Widget Trigs correctly inserted");
		else
			out.println("Error to insert new Widget Trigs");
		
		if(widgetUtils.addNewOengWMesgs(
				request.getParameter("newWidgetId"))
				)
			out.println("New Widget Messages Files It/En correctly inserted");
		else
			out.println("Error to insert new Widget Messages Files");
		
		if(widgetUtils.addNewOengWStyle(
				request.getParameter("newWidgetId"))
				)
			out.println("New Widget Style correctly inserted");
		else
			out.println("Error to insert new Widget Style");
		
		if(widgetUtils.addOengSqueueConf(
				request.getParameter("page_regex_squeue"), 
				request.getParameter("adcAddress"), 
				request.getParameter("newWidgetId"))
				)
			out.println("New Oeng Squeue correctly inserted");
		else
			out.println("Error to insert new Oeng Squeue");
		
		if(widgetUtils.addOengWInfServer(
				request.getParameter("newWidgetId"),
				Integer.parseInt(request.getParameter("infServerId")))
				)
			out.println("New Oeng Widget Infrastructure Server correctly inserted");
		else
			out.println("Error to insert new Oeng Widget Infrastructure Server");
		
		if(channelUtils.addHubAddress(
				request.getParameter("adcAddress"),
				"Y", 
				Integer.parseInt(request.getParameter("automatic")),
				Integer.parseInt(request.getParameter("max_transfer_attempt")))
				)
			out.println("New Channel ACD Address correctly inserted");
		else
			out.println("Error to insert new Channel ACD Address");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
