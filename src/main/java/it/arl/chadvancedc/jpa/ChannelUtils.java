package it.arl.chadvancedc.jpa;

import java.sql.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class ChannelUtils {
	private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("channel");
	
	public boolean addHubAddress(String address,/*String clusterNone,String createdBy,Date rowCreationDate,Date rowUpdateDate,*/
			String isAcd,Integer automatic,Integer maxTransferAttempt) {
		
		
		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();
            

            // Create a new Connection Channel Instance object
            HubAddress hubAddress = new HubAddress();

            hubAddress.setAddress(address);
            //hubAddress.setClusterNone(clusterNone);
            //hubAddress.setCreatedBy(createdBy);
            hubAddress.setRowCreationDate(new Date(System.currentTimeMillis()));
            hubAddress.setRowUpdateDate(new Date(System.currentTimeMillis()));
            hubAddress.setIsAcd(isAcd);
            hubAddress.setAutomatic(automatic);
            hubAddress.setMaxTransferAttempt(maxTransferAttempt);
            
            // Save the Connection Channel Instance object
            manager.persist(hubAddress);

            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		return true;
	}
	
}
