package it.arl.chadvancedc.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="oeng_widget_inf_servers")

public class OengWInfServ implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="WIDGET_ID", unique=true)
	private String widgetId;
	
	@Id
	@Column(name="SERVER_ID", unique=true)
	private Integer serverId;

	public String getWidgetId() {
		return widgetId;
	}

	public void setWidgetId(String widgetId) {
		this.widgetId = widgetId;
	}

	public Integer getServerId() {
		return serverId;
	}

	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}
	

}
