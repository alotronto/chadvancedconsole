package it.arl.chadvancedc.jpa;



import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


public class WidgetUtils {
	private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("connector");
	
	
	/**
	 * Il metodo esegue una query sulla abella oeng_widgetconfiguration_trigs 
	 * in modo da ritornare il max ID
	 * 
	 * @return int id 
	 */
	private int getLastTrigsID() {
		
		Integer lstid = -1;
		
		//Create an EntityManager
		EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		
		try {
			//Get a transaction
			transaction = manager.getTransaction();
			//Begin the transaction
			transaction.begin();
			
			lstid = manager.createQuery("Select max(s.id) "
					+ "from OengWTrigs s",Integer.class).getSingleResult();
			transaction.commit();
			
		}catch(Exception ex) {
			// If there are any exceptions, roll back the changes
			if (transaction != null) {
				transaction.rollback();
			}
			// Print the exception
			ex.printStackTrace();
		} finally {
			//Close the EntityManager
			manager.close();
		}
		return lstid.intValue();
	}
	
	private String getLastFileMessageOengWMsgs(Integer Id) {
		
		String result = null;
		
		//Create an EntityManager
		EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		
		try {
			//Get a transaction
			transaction = manager.getTransaction();
			//Begin the transaction
			transaction.begin();
			
			OengWMsg myOengWMsgs = manager.find(OengWMsg.class, Id);
			
			result = myOengWMsgs.getMessageFile();
			
			transaction.commit();
			
		}catch(Exception ex) {
			// If there are any exceptions, roll back the changes
			if (transaction != null) {
				transaction.rollback();
			}
			// Print the exception
			ex.printStackTrace();
		} finally {
			//Close the EntityManager
			manager.close();
		}
		return result;
	}
	
	private int getLastOengWMsgsId() {
		Integer lstid = -1;
		
		//Create an EntityManager
		EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		
		try {
			//Get a transaction
			transaction = manager.getTransaction();
			//Begin the transaction
			transaction.begin();
			
			lstid = manager.createQuery("Select max(s.id) "
					+ "from OengWMsg s",Integer.class).getSingleResult();
			transaction.commit();
			
		}catch(Exception ex) {
			// If there are any exceptions, roll back the changes
			if (transaction != null) {
				transaction.rollback();
			}
			// Print the exception
			ex.printStackTrace();
		} finally {
			//Close the EntityManager
			manager.close();
		}
		return lstid.intValue();
	}

	private int getLastOengWStyleId() {
		Integer lstid = -1;
		
		//Create an EntityManager
		EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		
		try {
			//Get a transaction
			transaction = manager.getTransaction();
			//Begin the transaction
			transaction.begin();
			
			lstid = manager.createQuery("Select max(s.id) "
					+ "from OengWStyle s",Integer.class).getSingleResult();
			transaction.commit();
			
		}catch(Exception ex) {
			// If there are any exceptions, roll back the changes
			if (transaction != null) {
				transaction.rollback();
			}
			// Print the exception
			ex.printStackTrace();
		} finally {
			//Close the EntityManager
			manager.close();
		}
		return lstid.intValue();
	}
	
	private int getLastOengSqueueId() {
		Integer lstid = -1;
		
		//Create an EntityManager
		EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		
		try {
			//Get a transaction
			transaction = manager.getTransaction();
			//Begin the transaction
			transaction.begin();
			
			lstid = manager.createQuery("Select max(s.id) "
					+ "from OengSqueueConf s",Integer.class).getSingleResult();
			transaction.commit();
			
		}catch(Exception ex) {
			// If there are any exceptions, roll back the changes
			if (transaction != null) {
				transaction.rollback();
			}
			// Print the exception
			ex.printStackTrace();
		} finally {
			//Close the EntityManager
			manager.close();
		}
		return lstid.intValue();
	}
	
	private String getLastStyleOengW(Integer Id) {
		
		String result = null;
		
		//Create an EntityManager
		EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		
		try {
			//Get a transaction
			transaction = manager.getTransaction();
			//Begin the transaction
			transaction.begin();
			
			OengWStyle myOengWStyle = manager.find(OengWStyle.class, Id);
			
			result = myOengWStyle.getCssFile();
			
			transaction.commit();
			
		}catch(Exception ex) {
			// If there are any exceptions, roll back the changes
			if (transaction != null) {
				transaction.rollback();
			}
			// Print the exception
			ex.printStackTrace();
		} finally {
			//Close the EntityManager
			manager.close();
		}
		return result;
	}
	
	public boolean addNewOengWMesgs(String wid) {
		
		int lstOengWMsgssId = getLastOengWMsgsId();
		String lstFileMessage = getLastFileMessageOengWMsgs(lstOengWMsgssId);
		
		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Create a new Oeng Widget Messages object
            OengWMsg oengWMsgsIt = new OengWMsg();
            
            oengWMsgsIt.setId(lstOengWMsgssId+1);
            oengWMsgsIt.setLanguage("it");
            oengWMsgsIt.setRowUpdateDate(new Date(System.currentTimeMillis()));
            oengWMsgsIt.setMessageFile(lstFileMessage);
            oengWMsgsIt.setOeChInstanceId(wid);
            
            OengWMsg oengWMsgsEn = new OengWMsg();
            oengWMsgsEn.setId(lstOengWMsgssId+2);
            oengWMsgsEn.setLanguage("en");
            oengWMsgsEn.setRowUpdateDate(new Date(System.currentTimeMillis()));
            oengWMsgsEn.setMessageFile(lstFileMessage);
            oengWMsgsEn.setOeChInstanceId(wid);
            // Save the Oeng Widget Trigers object
            
            manager.persist(oengWMsgsIt);
            manager.persist(oengWMsgsEn);
            
            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		return true;
		
	}
	
	public boolean addNewOengWStyle(String wid) {
		int lstOengWStyleId = getLastOengWStyleId();
		String lstcssFile = getLastStyleOengW(lstOengWStyleId);
		
		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Create a new Oeng Widget Style object
            OengWStyle newOengWStyle = new OengWStyle();
            // Save the Oeng Widget Style object
            
            newOengWStyle.setId(lstOengWStyleId+1);
            newOengWStyle.setCssFile(lstcssFile);
            newOengWStyle.setOeChInstanceId(wid);
            newOengWStyle.setRowUpdateDate(new Date(System.currentTimeMillis()));
            
            manager.persist(newOengWStyle);
            
            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		return true;
		
	}
	
	public boolean addNewTrigs (String wid, Integer activation_timeout, String page_regex) {
		
		int lstTrigsId = getLastTrigsID();
		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Create a new Oeng Widget Trigers object
            OengWTrigs newTrig = new OengWTrigs(lstTrigsId+1, activation_timeout, page_regex, wid);
            
            // Save the Oeng Widget Trigers object
            manager.persist(newTrig);

            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		return true;
	}

	public boolean addNewChInstance (String id, String acd_address, String alias, Integer hub_enabled, String type,
			String default_locale, String description, Integer enabled, String tenant_id) {
		
		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();
            

            // Create a new Connection Channel Instance object
            ConnChInst newChIstance = new ConnChInst(id,acd_address,alias,hub_enabled,type,
        			default_locale,description,enabled,tenant_id);
            
            // Save the Connection Channel Instance object
            manager.persist(newChIstance);

            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		return true;
	}

	public boolean addOengWidConf(Integer logged_ag_wait_cntcn_radio, Integer max_chat_workload_per_agent, Integer cache_eviction_enabled,
			Integer chat_transcription_enabled, Integer cross_domain_enabled, Integer hideonmobile, Integer hideoffline, String origin_site,
			Integer satisfaction_range, Integer showcollapsed, Integer site_activation_timeout, Integer timeout_min, String wId,
			String offline_ch_instance_id,Integer oeng_calendar_id, Integer upgrade_video_enabled, Integer upgrade_audio_enabled, Integer direct_video_enabled,
			Integer direct_audio_enabled, Integer message_audio_notif_enabled, Integer co_browsing_enabled, Integer automatically,
			Integer fields_metadata) {

		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;
        
        
        try {
            // Get a transaction
        	
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            // Create a new Widget Configuration Object
            OengWConf oengWidgetConf = new OengWConf();
            
            oengWidgetConf.setLogged_ag_wait_cntcn_radio(logged_ag_wait_cntcn_radio);
            oengWidgetConf.setMax_chat_workload_per_agent(max_chat_workload_per_agent);
            oengWidgetConf.setCache_eviction_enabled(cache_eviction_enabled);
            oengWidgetConf.setChat_transcription_enabled(chat_transcription_enabled);
            oengWidgetConf.setCross_domain_enabled(cross_domain_enabled);
            oengWidgetConf.setHideonmobile(hideonmobile);
            oengWidgetConf.setHideoffline(hideoffline);
            oengWidgetConf.setOrigin_site(origin_site);
            oengWidgetConf.setSatisfaction_range(satisfaction_range);
            oengWidgetConf.setShowcollapsed(showcollapsed);
            oengWidgetConf.setSite_activation_timeout(site_activation_timeout);
            oengWidgetConf.setTimeout_min(timeout_min);
            oengWidgetConf.setId(wId);
            oengWidgetConf.setOffline_ch_instance_id(offline_ch_instance_id);
            oengWidgetConf.setOeng_calendar_id(oeng_calendar_id);
            oengWidgetConf.setUpgrade_video_enabled(upgrade_video_enabled);
            oengWidgetConf.setUpgrade_audio_enabled(upgrade_audio_enabled);
            oengWidgetConf.setDirect_video_enabled(direct_video_enabled);
            oengWidgetConf.setDirect_audio_enabled(direct_audio_enabled);
            oengWidgetConf.setMessage_audio_notif_enabled(message_audio_notif_enabled);
            oengWidgetConf.setCo_browsing_enabled(co_browsing_enabled);
            oengWidgetConf.setAutomatically(automatically);
            oengWidgetConf.setRow_update_date(new Date(System.currentTimeMillis()));
            oengWidgetConf.setFields_metadata(fields_metadata);
            
            // Save the Widget Configuration Object
            manager.persist(oengWidgetConf);

            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		
		return true;
	}

	public boolean addOengSqueueConf(String page_reg_exp, String service_queue, String oeng_widgetconfiguration_id) {
		
		int lstOengSqueueId = getLastOengSqueueId();
		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Create a new Oeng  Squeue Configuration object
            OengSqueueConf oengSqueueConf = new OengSqueueConf();
            
            oengSqueueConf.setId(lstOengSqueueId+1);
            oengSqueueConf.setOengWConfigurationId(oeng_widgetconfiguration_id);
            oengSqueueConf.setPageReg(page_reg_exp);
            oengSqueueConf.setServiceQueue(service_queue);
            
            // Save the Oeng Widget Trigers object
            manager.persist(oengSqueueConf);

            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		return true;
	}

	public boolean addOengWInfServer(String widgetId, Integer serverId) {

		// Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Create a new Oeng Widget Infrastructure Server object
            OengWInfServ oengWInfServ = new OengWInfServ();
            
            oengWInfServ.setServerId(serverId);
            oengWInfServ.setWidgetId(widgetId);
            
            // Save the Oeng Widget Infrastructure Server object
            manager.persist(oengWInfServ);

            // Commit the transaction
            transaction.commit();
        }catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
            return false;
        } finally {
            // Close the EntityManager
            manager.close();
        }
		return true;
	}
}
