package it.arl.chadvancedc.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="oeng_squeue_config")
public class OengSqueueConf implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID", unique=true)
	private Integer id;
	
	@Column(name="PAGE_REG_EXP", nullable=false)
	private String pageReg;
	
	@Column(name="SERVICE_QUEUE", nullable=false)
	private String serviceQueue;
	
	@Column(name="OENG_WIDGETCONFIGURATION_ID", nullable=false)
	private String oengWConfigurationId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPageReg() {
		return pageReg;
	}

	public void setPageReg(String pageReg) {
		this.pageReg = pageReg;
	}

	public String getServiceQueue() {
		return serviceQueue;
	}

	public void setServiceQueue(String serviceQueue) {
		this.serviceQueue = serviceQueue;
	}

	public String getOengWConfigurationId() {
		return oengWConfigurationId;
	}

	public void setOengWConfigurationId(String oengWConfigurationId) {
		this.oengWConfigurationId = oengWConfigurationId;
	}
	
}
