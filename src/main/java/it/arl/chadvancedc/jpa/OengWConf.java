package it.arl.chadvancedc.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="oeng_widgetconfiguration")
public class OengWConf implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "ID", unique = true)
    private String id;
	
	@Column(name="LOGGED_AG_WAIT_CNTCT_RATIO", nullable = true)
	private Integer logged_ag_wait_cntcn_radio;
	
	@Column(name="LAST_KNOWN_CONVERSATION_ID", nullable = true)
	private Integer last_known_conversation_id;
	
	@Column(name="MAX_CHAT_WORKLOAD_PER_AGENT", nullable = true)
	private Integer max_chat_workload_per_agent;
	
	@Column(name="AGENTS_AVAILABLE", nullable = true)
	private Integer agents_available;
	
	@Column(name="AGENTS_REGISTERED", nullable = true)
	private Integer agents_registered;
	
	@Column(name="CONTACT_QUEUED", nullable = true)
	private Integer contact_queued;
	
	@Column(name="CACHE_EVICTION_ENABLED", nullable = true)
	private Integer cache_eviction_enabled;
	
	@Column(name="CHAT_FORMAT_FILE", nullable = true)
	private String chat_format_file;
	
	@Column(name="CHAT_TRANSCRIPTION_ENABLED", nullable = true)
	private Integer chat_transcription_enabled;
	
	@Column(name="CROSS_DOMAIN_ENABLED", nullable = true)
	private Integer cross_domain_enabled;
	
	@Column(name="HIDEONMOBILE", nullable = true)
	private Integer hideonmobile;
	
	@Column(name="HIDEOFFLINE", nullable = true)
	private Integer hideoffline;
	
	@Column(name="ORIGIN_SITE", nullable = true)
	private String origin_site;
	
	@Column(name="SATISFACTION_RANGE", nullable = true)
	private Integer satisfaction_range;
	
	@Column(name="SHOWCOLLAPSED", nullable = true)
	private Integer showcollapsed;
	
	@Column(name="SITE_ACTIVATION_TIMEOUT", nullable = true)
	private Integer site_activation_timeout;

	@Column(name="TIMEOUT_MIN", nullable = true)
	private Integer timeout_min;
	
	@Column(name="ROW_UPDATE_DATE", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date row_update_date;
	
	@Column(name="UP_MAX_FILE_SIZE", nullable = true)
	private Integer up_max_file_size;
	
	@Column(name="UP_MAX_REQUEST_SIZE", nullable = true)
	private Integer up_max_request_size;
	
	@Column(name="UP_M_THRESHOLD", nullable = true)
	private Integer up_m_threshold;

	@Column(name="CALLMEBACK_CH_INSTANCE_ID", nullable = true)
	private String callmeback_ch_instance_id;
	
	@Column(name="FIELDS_METADATA", nullable = true)
	private Integer fields_metadata;
	
	@Column(name="OFFLINE_CH_INSTANCE_ID", nullable = true)
	private String offline_ch_instance_id;
	
	@Column(name="OENG_CALENDAR_ID", nullable = true)
	private Integer oeng_calendar_id;
	
	@Column(name="UPGRADE_VIDEO_ENABLED", nullable = true)
	private Integer upgrade_video_enabled;
	
	@Column(name="UPGRADE_AUDIO_ENABLED", nullable = true)
	private Integer upgrade_audio_enabled;
	
	@Column(name="DIRECT_VIDEO_ENABLED", nullable = true)
	private Integer direct_video_enabled;
	
	@Column(name="DIRECT_AUDIO_ENABLED", nullable = true)
	private Integer direct_audio_enabled;
	
	@Column(name="MESSAGE_AUDIO_NOTIF_ENABLED", nullable = true)
	private Integer message_audio_notif_enabled;
	
	@Column(name="CO_BROWSING_ENABLED", nullable = true)
	private Integer co_browsing_enabled;
	
	@Column(name="AUTOMATICALLY", nullable = true)
	private Integer automatically;
	
	@Column(name="SESSION_EXPIRATION", nullable = true)
	private Integer session_expiration;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getLogged_ag_wait_cntcn_radio() {
		return logged_ag_wait_cntcn_radio;
	}

	public void setLogged_ag_wait_cntcn_radio(Integer logged_ag_wait_cntcn_radio) {
		this.logged_ag_wait_cntcn_radio = logged_ag_wait_cntcn_radio;
	}

	public Integer getLast_known_conversation_id() {
		return last_known_conversation_id;
	}

	public void setLast_known_conversation_id(Integer last_known_conversation_id) {
		this.last_known_conversation_id = last_known_conversation_id;
	}

	public Integer getMax_chat_workload_per_agent() {
		return max_chat_workload_per_agent;
	}

	public void setMax_chat_workload_per_agent(Integer max_chat_workload_per_agent) {
		this.max_chat_workload_per_agent = max_chat_workload_per_agent;
	}

	public Integer getAgents_available() {
		return agents_available;
	}

	public void setAgents_available(Integer agents_available) {
		this.agents_available = agents_available;
	}

	public Integer getAgents_registered() {
		return agents_registered;
	}

	public void setAgents_registered(Integer agents_registered) {
		this.agents_registered = agents_registered;
	}

	public Integer getContact_queued() {
		return contact_queued;
	}

	public void setContact_queued(Integer contact_queued) {
		this.contact_queued = contact_queued;
	}

	public Integer getCache_eviction_enabled() {
		return cache_eviction_enabled;
	}

	public void setCache_eviction_enabled(Integer cache_eviction_enabled) {
		this.cache_eviction_enabled = cache_eviction_enabled;
	}

	public String getChat_format_file() {
		return chat_format_file;
	}

	public void setChat_format_file(String chat_format_file) {
		this.chat_format_file = chat_format_file;
	}

	public Integer getChat_transcription_enabled() {
		return chat_transcription_enabled;
	}

	public void setChat_transcription_enabled(Integer chat_transcription_enabled) {
		this.chat_transcription_enabled = chat_transcription_enabled;
	}

	public Integer getCross_domain_enabled() {
		return cross_domain_enabled;
	}

	public void setCross_domain_enabled(Integer cross_domain_enabled) {
		this.cross_domain_enabled = cross_domain_enabled;
	}

	public Integer getHideonmobile() {
		return hideonmobile;
	}

	public void setHideonmobile(Integer hideonmobile) {
		this.hideonmobile = hideonmobile;
	}

	public Integer getHideoffline() {
		return hideoffline;
	}

	public void setHideoffline(Integer hideoffline) {
		this.hideoffline = hideoffline;
	}

	public String getOrigin_site() {
		return origin_site;
	}

	public void setOrigin_site(String origin_site) {
		this.origin_site = origin_site;
	}

	public Integer getSatisfaction_range() {
		return satisfaction_range;
	}

	public void setSatisfaction_range(Integer satisfaction_range) {
		this.satisfaction_range = satisfaction_range;
	}

	public Integer getShowcollapsed() {
		return showcollapsed;
	}

	public void setShowcollapsed(Integer showcollapsed) {
		this.showcollapsed = showcollapsed;
	}

	public Integer getSite_activation_timeout() {
		return site_activation_timeout;
	}

	public void setSite_activation_timeout(Integer site_activation_timeout) {
		this.site_activation_timeout = site_activation_timeout;
	}

	public Integer getTimeout_min() {
		return timeout_min;
	}

	public void setTimeout_min(Integer timeout_min) {
		this.timeout_min = timeout_min;
	}

	public Date getRow_update_date() {
		return row_update_date;
	}

	public void setRow_update_date(Date row_update_date) {
		this.row_update_date = row_update_date;
	}

	public Integer getUp_max_file_size() {
		return up_max_file_size;
	}

	public void setUp_max_file_size(Integer up_max_file_size) {
		this.up_max_file_size = up_max_file_size;
	}

	public Integer getUp_max_request_size() {
		return up_max_request_size;
	}

	public void setUp_max_request_size(Integer up_max_request_size) {
		this.up_max_request_size = up_max_request_size;
	}

	public Integer getUp_m_threshold() {
		return up_m_threshold;
	}

	public void setUp_m_threshold(Integer up_m_threshold) {
		this.up_m_threshold = up_m_threshold;
	}

	public String getCallmeback_ch_instance_id() {
		return callmeback_ch_instance_id;
	}

	public void setCallmeback_ch_instance_id(String callmeback_ch_instance_id) {
		this.callmeback_ch_instance_id = callmeback_ch_instance_id;
	}

	public Integer getFields_metadata() {
		return fields_metadata;
	}

	public void setFields_metadata(Integer fields_metadata) {
		this.fields_metadata = fields_metadata;
	}

	public String getOffline_ch_instance_id() {
		return offline_ch_instance_id;
	}

	public void setOffline_ch_instance_id(String offline_ch_instance_id) {
		this.offline_ch_instance_id = offline_ch_instance_id;
	}

	public Integer getOeng_calendar_id() {
		return oeng_calendar_id;
	}

	public void setOeng_calendar_id(Integer oeng_calendar_id) {
		this.oeng_calendar_id = oeng_calendar_id;
	}

	public Integer getUpgrade_video_enabled() {
		return upgrade_video_enabled;
	}

	public void setUpgrade_video_enabled(Integer upgrade_video_enabled) {
		this.upgrade_video_enabled = upgrade_video_enabled;
	}

	public Integer getUpgrade_audio_enabled() {
		return upgrade_audio_enabled;
	}

	public void setUpgrade_audio_enabled(Integer upgrade_audio_enabled) {
		this.upgrade_audio_enabled = upgrade_audio_enabled;
	}

	public Integer getDirect_video_enabled() {
		return direct_video_enabled;
	}

	public void setDirect_video_enabled(Integer direct_video_enabled) {
		this.direct_video_enabled = direct_video_enabled;
	}

	public Integer getDirect_audio_enabled() {
		return direct_audio_enabled;
	}

	public void setDirect_audio_enabled(Integer direct_audio_enabled) {
		this.direct_audio_enabled = direct_audio_enabled;
	}

	public Integer getMessage_audio_notif_enabled() {
		return message_audio_notif_enabled;
	}

	public void setMessage_audio_notif_enabled(Integer message_audio_notif_enabled) {
		this.message_audio_notif_enabled = message_audio_notif_enabled;
	}

	public Integer getCo_browsing_enabled() {
		return co_browsing_enabled;
	}

	public void setCo_browsing_enabled(Integer co_browsing_enabled) {
		this.co_browsing_enabled = co_browsing_enabled;
	}

	public Integer getAutomatically() {
		return automatically;
	}

	public void setAutomatically(Integer automatically) {
		this.automatically = automatically;
	}

	public Integer getSession_expiration() {
		return session_expiration;
	}

	public void setSession_expiration(Integer session_expiration) {
		this.session_expiration = session_expiration;
	}
	
	
	
}
