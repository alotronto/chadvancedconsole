package it.arl.chadvancedc.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="oeng_widgetconfiguration_trigs")

public class OengWTrigs implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ID", unique = true)
    private Integer id;
	
	@Column(name="ACTIVATION_TIMEOUT", nullable = false)
	private Integer activation_timeout;
	
	@Column(name="PAGE_REGEX", nullable = false)
	private String page_regex;
	
	@Column(name="OENG_WIDGETCONFIGURATION_ID", nullable = false)
	private String oeng_widgetconfiguration_id;
	
	public OengWTrigs(Integer id, Integer activation_timeout ,String page_regex,  
			String oeng_widgetconfiguration_id) {
		this.id = id;
		this.activation_timeout = activation_timeout;
		this.page_regex = page_regex;
		this.oeng_widgetconfiguration_id = oeng_widgetconfiguration_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActivation_timeout() {
		return activation_timeout;
	}

	public void setActivation_timeout(int activation_timeout) {
		this.activation_timeout = activation_timeout;
	}

	public String getPage_regex() {
		return page_regex;
	}

	public void setPage_regex(String page_regex) {
		this.page_regex = page_regex;
	}

	public String getOeng_widgetconfiguration_id() {
		return oeng_widgetconfiguration_id;
	}

	public void setOeng_widgetconfiguration_id(String oeng_widgetconfiguration_id) {
		this.oeng_widgetconfiguration_id = oeng_widgetconfiguration_id;
	}
	
	
	

}
