package it.arl.chadvancedc.jpa;

public interface WidgetOperation {
	
	public boolean addWidget(String widgetId);

}
