package it.arl.chadvancedc.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hub_addresses")
public class HubAddress implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ADDRESS", unique=true)
	private String address;
	
	@Column(name="CLUSTER_NODE", nullable=true)
	private String clusterNone;
	
	@Column(name="CREATED_BY", nullable=true)
	private String createdBy;
	
	@Column(name="ROW_CREATION_DATE", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date rowCreationDate;
	
	@Column(name="UPDATE_BY", nullable=true)
	private String updateBy;
	@Column(name="UPDATE_BY_THREAD", nullable=true)
	private String updateByThread;
	
	@Column(name="ROW_UPDATE_DATE", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date rowUpdateDate;
	
	@Column(name="IS_ACD", nullable=false)
	private String isAcd;
	@Column(name="AUTOMATIC", nullable=true)
	private Integer automatic;
	@Column(name="MAX_TRANSFER_ATTEMPT", nullable=true)
	private Integer maxTransferAttempt;
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getClusterNone() {
		return clusterNone;
	}
	public void setClusterNone(String clusterNone) {
		this.clusterNone = clusterNone;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getRowCreationDate() {
		return rowCreationDate;
	}
	public void setRowCreationDate(Date rowCreationDate) {
		this.rowCreationDate = rowCreationDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getUpdateByThread() {
		return updateByThread;
	}
	public void setUpdateByThread(String updateByThread) {
		this.updateByThread = updateByThread;
	}
	public Date getRowUpdateDate() {
		return rowUpdateDate;
	}
	public void setRowUpdateDate(Date rowUpdateDate) {
		this.rowUpdateDate = rowUpdateDate;
	}
	public String getIsAcd() {
		return isAcd;
	}
	public void setIsAcd(String isAcd) {
		this.isAcd = isAcd;
	}
	public Integer getAutomatic() {
		return automatic;
	}
	public void setAutomatic(Integer automatic) {
		this.automatic = automatic;
	}
	public Integer getMaxTransferAttempt() {
		return maxTransferAttempt;
	}
	public void setMaxTransferAttempt(Integer maxTransferAttempt) {
		this.maxTransferAttempt = maxTransferAttempt;
	}
}

