package it.arl.chadvancedc.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="oeng_widgetconfiguration_msgs")
public class OengWMsg implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID", unique=true)
	private Integer id;
	
	@Column(name="LANGUAGE", nullable=false)
	private String language;
	
	@Column(name="MESSAGE_FILE", nullable=true)
	private String messageFile;
	
	@Column(name="ROW_UPDATE_DATE", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date rowUpdateDate;
	
	@Column(name="OE_CH_INSTANCE_ID", nullable=false)
	private String oeChInstanceId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMessageFile() {
		return messageFile;
	}

	public void setMessageFile(String messageFile) {
		this.messageFile = messageFile;
	}

	public Date getRowUpdateDate() {
		return rowUpdateDate;
	}

	public void setRowUpdateDate(Date rowUpdateDate) {
		this.rowUpdateDate = rowUpdateDate;
	}

	public String getOeChInstanceId() {
		return oeChInstanceId;
	}

	public void setOeChInstanceId(String oeChInstanceId) {
		this.oeChInstanceId = oeChInstanceId;
	}
	

}
