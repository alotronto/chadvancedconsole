package it.arl.chadvancedc.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="connectors_ch_instance")
public class ConnChInst implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID", unique=true)
	private String id;
	
	@Column(name="ACD_ADDRESS", nullable=true)
	private String acd_address;
	
	@Column(name="ALIAS", nullable=true)
	private String alias;
	
	@Column(name="HUB_ENABLED", nullable=true)
	private Integer hub_enabled;
	
	@Column(name="TYPE", nullable=true)
	private String type;
	
	@Column(name="DEFAULT_LOCALE", nullable=true)
	private String default_locale;
	
	@Column(name="DESCRIPTION", nullable=true)
	private String description;
	
	@Column(name="ENABLED", nullable=true)
	private Integer enabled;
	
	@Column(name="TENANT_ID", nullable=true)
	private String tenant_id;
	
	
	
	
	public ConnChInst(String id, String acd_address, String alias, Integer hub_enabled, String type,
			String default_locale, String description, Integer enabled, String tenant_id) {
		super();
		this.id = id;
		this.acd_address = acd_address;
		this.alias = alias;
		this.hub_enabled = hub_enabled;
		this.type = type;
		this.default_locale = default_locale;
		this.description = description;
		this.enabled = enabled;
		this.tenant_id = tenant_id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAcd_address() {
		return acd_address;
	}

	public void setAcd_address(String acd_address) {
		this.acd_address = acd_address;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Integer getHub_enabled() {
		return hub_enabled;
	}

	public void setHub_enabled(Integer hub_enabled) {
		this.hub_enabled = hub_enabled;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDefault_locale() {
		return default_locale;
	}

	public void setDefault_locale(String default_locale) {
		this.default_locale = default_locale;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}
	
	
	

}
